resource "newrelic_cloud_aws_link_account" "foo" {
  arn = "arn:aws:iam::842919758536:role/NewRelicInfrastructure-Integrations-francis"

  metric_collection_mode = "PULL"
  name = "NR-aws-Acoount"
}
resource "newrelic_cloud_aws_integrations" "bar" {
  linked_account_id = newrelic_cloud_aws_link_account.foo.id
  billing {}
  cloudtrail {
    metrics_polling_interval = 6000
    aws_regions              = ["us-east-1", "us-east-2"]
  }
  health {
    metrics_polling_interval = 6000
  }
  //trusted_advisor {
  //  metrics_polling_interval = 6000
  //}
  lambda {
    aws_regions              = ["us-east-1"]
    fetch_tags               = true
    metrics_polling_interval = 6000
    tag_key                  = "owner"
    tag_value                = "francis isabel"
  }
}
